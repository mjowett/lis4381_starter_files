- remove *all* "_student" references in file/directory names

**For example:**
    - a2_student_files becomes a2
    - css_student_files becomes css
    - global_student_files becomes global
    - etc.
